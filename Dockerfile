############################################################
# Dockerfile to build NFS Server 
# Based on Ubuntu 14.04
############################################################

# Set the base image to Ubuntu
FROM khemlabs/ubuntu-essential:latest

# File Author / Maintainer
MAINTAINER Maintaner khemlabs

RUN apt-get update -qq && apt-get install -y nfs-common inotify-tools -qq
